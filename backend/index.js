const app = require("express")()
const cors = require("cors")
const got = require("got")

app.use(cors())

app.post("/apireq", async (req, res) => {
    const response = await got('https://wallhaven.cc/api/v1/search');
    res.json(response.body);

})

app.post("/apireq/:id", async (req, res) =>{
    const response = await got('https://wallhaven.cc/api/v1/w/' + req.params.id);
    res.json(response.body);
})


app.post("/apisearch/:query", async (req, res) =>{
    const response = await got('https://wallhaven.cc/api/v1/search?q=' + req.params.query);
    res.json(response.body);
})

app.listen(12321, () => {
    console.log('Server ready')
})