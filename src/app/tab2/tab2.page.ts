import { Component, OnInit } from '@angular/core';

var search = "";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  datao = [];

  constructor() {
    
  }

  ngOnInit(){
    const searchbar = document.querySelector('ion-searchbar');
    searchbar.addEventListener('ionInput', this.handleInput);
  }

  handleInput(event){
    search = event.target.value.toLowerCase();
    console.log(search);
  }

  async getData(){
    const api_url = 'http://localhost:12321'
    let output;
    const url = api_url + "/apisearch/" + search;
    console.log(search);
    console.log(url);
    const res = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.json())
    .then(json => output = json)
    output = JSON.parse(output).data;
    console.log(output);
    this.datao = output;
    //return output;
  }

  
  
  Search(){
    this.getData();
  }
}
