import { Component, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  datao;

  constructor(private http:HttpClient) { 
    //this.getData().subscribe(data=>{
    //  this.datao = data;
    //})
    this.getData();
    //this.datao = this.getData();
  }
  async getData(){
    const api_url = 'http://localhost:12321'
    let output;
    const res = await fetch(api_url + '/apireq', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.json())
    .then(json => output = json)
    output = JSON.parse(output).data;
    console.log(output);
    this.datao = output;
    //return output;
  }

  async getDataProd(){
    const api_url = 'https://wallhaven.cc/api/v1/search'
    let output;
    const res = await fetch(api_url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.json())
    .then(json => output = json)
    output = JSON.parse(output).data;
    console.log(output);
    this.datao = output;
    //return output;
  }

}
