import { Component } from '@angular/core';
import { FileTransfer, FileTransferObject } from '@awesome-cordova-plugins/file-transfer/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(private transfer: FileTransfer, private file: File) {}


  SaveImage(){
    const url = "https://w.wallhaven.cc/full/4d/wallhaven-4dywrj.jpg";
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(url, this.file.externalDataDirectory + "demo" + ".jpg").then((entry) =>{
      console.log("Pog");
      console.log('download complete: ' + entry.toURL());
    },(err)=>{
      console.log("Uh oh...");
    })
  }
}
