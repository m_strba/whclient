import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileTransfer, FileTransferObject } from '@awesome-cordova-plugins/file-transfer/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  id = "";
  datao = [];
  url = "";
  constructor(private activatedRoute: ActivatedRoute, private transfer: FileTransfer, private file: File) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap =>{
      this.id = paramMap.get('wpID');
    })
    this.getData();
  }
  async getData(){
    const api_url = 'http://localhost:12321'
    let output;
    const res = await fetch(api_url + '/apireq/' + this.id, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.json())
    .then(json => output = json)
    output = JSON.parse(output).data;
    console.log(output);
    this.url = output.url;
    this.datao = output;
    //return output;
  }

  async getDataProd(){
    const api_url = 'https://wallhaven.cc/api/v1/w/'
    let output;
    const res = await fetch(api_url + this.id, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.json())
    .then(json => output = json)
    output = JSON.parse(output).data;
    console.log(output);
    this.url = output.url;
    this.datao = output;
    //return output;
  }

  SaveImage(){;
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(this.url, this.file.dataDirectory + this.id + ".png").then((entry) =>{
      console.log("Pog");
    },(err)=>{
      console.log("Uh oh...");
    })
  }
}
